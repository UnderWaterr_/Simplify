package ru.itis.simplify.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.simplify.dto.UserDto;
import ru.itis.simplify.model.Recipe;
import ru.itis.simplify.model.RecipeComment;
import ru.itis.simplify.model.User;
import ru.itis.simplify.service.inter.RecipeCommentService;
import ru.itis.simplify.service.inter.RecipeService;
import ru.itis.simplify.service.inter.UserService;

import javax.servlet.http.HttpSession;

@Controller
public class RecipeCommentController {

    private final RecipeCommentService recipeCommentService;
    private final RecipeService recipeService;
    private final UserService userService;

    @Autowired
    public RecipeCommentController(RecipeCommentService recipeCommentService, RecipeService recipeService,
                                   UserService userService) {
        this.recipeCommentService = recipeCommentService;
        this.recipeService = recipeService;
        this.userService = userService;
    }

    @PostMapping("/createRecipeComment/{recipeId}/{isMyRecipes}")
    public String saveRecipeComment(@RequestParam("comment") String comment,
                                    @PathVariable("recipeId") Integer recipeId,
                                    @PathVariable("isMyRecipes") Integer isMyRecipe, HttpSession session) {

        UserDto userDto = (UserDto) session.getAttribute("user");
        User user = userService.getRawUserByEmail(userDto.getEmail());
        Recipe recipe = recipeService.getRawRecipeById(recipeId);

        RecipeComment recipeComment = new RecipeComment(comment, user, recipe);
        recipeCommentService.save(recipeComment);

        return "redirect:/detailRecipe/" + recipeId + "/" + isMyRecipe;
    }
}
