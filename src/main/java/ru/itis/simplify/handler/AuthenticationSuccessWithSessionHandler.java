package ru.itis.simplify.handler;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import ru.itis.simplify.dto.UserDto;
import ru.itis.simplify.service.inter.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class AuthenticationSuccessWithSessionHandler extends
        SavedRequestAwareAuthenticationSuccessHandler
        implements AuthenticationSuccessHandler {

    private final UserService userService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException,
            ServletException {

        String email = authentication.getName();
        UserDto user = userService.getUserByEmail(email);
        request.getSession().setAttribute("user", user);
        request.getSession().removeAttribute("authenticationFailure");

        response.sendRedirect("/account");
    }
}

