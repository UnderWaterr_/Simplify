package ru.itis.simplify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.simplify.model.RecipeComment;

import java.util.List;


public interface RecipeCommentRepository extends JpaRepository<RecipeComment, Integer> {

    @Query(value = "select u from RecipeComment u where u.recipe.id = :recipeId")
    List<RecipeComment> getAllByRecipeId(@Param("recipeId") Integer recipeId);
}
