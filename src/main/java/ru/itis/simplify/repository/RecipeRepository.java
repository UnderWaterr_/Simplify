package ru.itis.simplify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.simplify.model.Recipe;

import java.util.List;


public interface RecipeRepository extends JpaRepository<Recipe, Integer> {

    List<Recipe> getAllByTitleContains(String title);

    List<Recipe> getAllByTitleContainsAndUserId(String title, Integer id);

    @Query(value = "select u from Recipe u where u.user.id = :userId")
    List<Recipe> getAllByUserId(@Param("userId") Integer userId);

    void deleteRecipeById(Integer id);
}
