package ru.itis.simplify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.simplify.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> getUserByEmail(String email);

    List<User> getAllByNicknameContains(String nickname);

    void deleteUserById(Integer userId);
}
