package ru.itis.simplify.service.inter;

import org.springframework.web.multipart.MultipartFile;
import ru.itis.simplify.dto.CreateUserDto;
import ru.itis.simplify.dto.UserDto;
import ru.itis.simplify.model.User;

import java.util.List;

public interface UserService {

    UserDto save(CreateUserDto user);

    List<UserDto> getAllUsers();

    List<UserDto> getUsersByNicknameLike(String nickname);

    UserDto getUserById(int userId);

    UserDto getUserByEmail(String email);

    User getRawUserByEmail(String email);

    UserDto updateUser(UserDto user);

    UserDto updateAvatar(MultipartFile file, UserDto userDto);

    void deleteUser(Integer userId);
}
