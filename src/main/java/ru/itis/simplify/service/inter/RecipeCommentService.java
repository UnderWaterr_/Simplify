package ru.itis.simplify.service.inter;

import ru.itis.simplify.dto.RecipeCommentDto;
import ru.itis.simplify.model.RecipeComment;

import java.util.List;

public interface RecipeCommentService {

    RecipeCommentDto save(RecipeComment comment);

    List<RecipeCommentDto> getAllByRecipeId(int recipeId);
}
