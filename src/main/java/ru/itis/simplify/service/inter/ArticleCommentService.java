package ru.itis.simplify.service.inter;

import ru.itis.simplify.dto.ArticleCommentDto;
import ru.itis.simplify.model.ArticleComment;

import java.util.List;

public interface ArticleCommentService {

    ArticleCommentDto save(ArticleComment comment);

    List<ArticleCommentDto> getAllByArticleId(int articleId);
}
