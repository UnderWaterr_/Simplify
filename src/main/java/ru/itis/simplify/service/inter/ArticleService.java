package ru.itis.simplify.service.inter;

import ru.itis.simplify.dto.ArticleDto;
import ru.itis.simplify.model.Article;

import java.util.List;

public interface ArticleService {

    ArticleDto save(Article article);

    ArticleDto getArticleById(int articleId);

    Article getRawArticleById(int articleId);

    List<ArticleDto> getAll();

    List<ArticleDto> getArticlesByTitleLike(String title);

    List<ArticleDto> getArticlesByTitleLikeAndUserId(String title, Integer id);

    List<ArticleDto> getAllByUserId(int userId);

    void deleteArticleById(int id);
}
