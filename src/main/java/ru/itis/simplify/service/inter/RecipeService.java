package ru.itis.simplify.service.inter;

import ru.itis.simplify.dto.RecipeDto;
import ru.itis.simplify.model.Recipe;

import java.util.List;

public interface RecipeService {

    RecipeDto save(Recipe recipe);

    RecipeDto getRecipeById(int recipeId);

    Recipe getRawRecipeById(int recipeId);

    List<RecipeDto> getAll();

    List<RecipeDto> getRecipesByTitleLike(String title);

    List<RecipeDto> getAllByUserId(int userId);

    List<RecipeDto> getRecipesByTitleLikeAndUserId(String title, Integer id);

    void deleteRecipeById(int id);
}
