package ru.itis.simplify.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.simplify.dto.RecipeCommentDto;
import ru.itis.simplify.model.RecipeComment;
import ru.itis.simplify.repository.RecipeCommentRepository;
import ru.itis.simplify.service.inter.RecipeCommentService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecipeCommentServiceImpl implements RecipeCommentService {

    private final RecipeCommentRepository recipeCommentRepository;

    @Autowired
    public RecipeCommentServiceImpl(RecipeCommentRepository recipeCommentRepository) {
        this.recipeCommentRepository = recipeCommentRepository;
    }

    @Override
    public RecipeCommentDto save(RecipeComment comment) {
        return RecipeCommentDto.fromModel(recipeCommentRepository.save(comment));
    }

    @Override
    public List<RecipeCommentDto> getAllByRecipeId(int recipeId) {
        List<RecipeComment> allComments = recipeCommentRepository.getAllByRecipeId(recipeId);

        return allComments.stream()
                .map(RecipeCommentDto::fromModel)
                .collect(Collectors.toList());
    }
}
