package ru.itis.simplify.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.simplify.dto.ArticleCommentDto;
import ru.itis.simplify.model.ArticleComment;
import ru.itis.simplify.repository.ArticleCommentRepository;
import ru.itis.simplify.service.inter.ArticleCommentService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArticleCommentServiceImpl implements ArticleCommentService {

    private final ArticleCommentRepository articleCommentRepository;

    @Autowired
    public ArticleCommentServiceImpl(ArticleCommentRepository articleCommentRepository) {
        this.articleCommentRepository = articleCommentRepository;
    }

    @Override
    public ArticleCommentDto save(ArticleComment comment) {
        return ArticleCommentDto.fromModel(articleCommentRepository.save(comment));
    }

    @Override
    public List<ArticleCommentDto> getAllByArticleId(int articleId) {
        List<ArticleComment> allComments = articleCommentRepository.getAllByArticleId(articleId);

        return allComments.stream()
                .map(ArticleCommentDto::fromModel)
                .collect(Collectors.toList());
    }
}
